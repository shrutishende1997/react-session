import react,{ Component } from "react";

class Section extends Component{
render(){

    return(
        <>
        <h3>What is HTML?</h3>
        <p>
        HTML stands for <strong>H</strong>yper<strong>T</strong>ext
        <strong>M</strong>arkup <strong>L</strong>anguage. It's a markup
        language that web developers use to structure and describe the content
        of a webpage (not a programming language).
        </p>
        <p>
        HTML consists of elements that describe different types of content:
        paragraphs, links, headings, images, video, etc. Web browsers
        understand HTML and render HTML code as websites.
        </p>
        <p>In HTML, each element is made up of 3 parts:</p>

        <ol>
        <li class="first-li">The opening tag</li>
        <li>The closing tag</li>
        <li>The actual element</li>
        </ol>

        <p>
        You can learn more at
        <a
            href="https://developer.mozilla.org/en-US/docs/Web/HTML"
            target="_blank"
            >MDN Web Docs</a
        >.
        </p>
        </>

    )


}


}
export default Section