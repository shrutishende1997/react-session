import { Component } from "react";

class Section2 extends Component{
    render(){

        return(<>


            <h3>Why should you learn HTML?</h3>

            <p>
            There are countless reasons for learning the fundamental language of
            the web. Here are 5 of them:
            </p>

            <ul>
            <li class="first-li">
                To be able to use the fundamental web dev language
            </li>
            <li>
                To hand-craft beautiful websites instead of relying on tools like
                Worpress or Wix
            </li>
            <li>To build web applications</li>
            <li>To impress friends</li>
            <li>To have fun 😃</li>
            </ul>

            <p>Hopefully you learned something new here. See you next time!</p>
            
        
        </>);

    }

}