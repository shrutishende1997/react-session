import React,{ Component } from 'react';
class Aside extends Component{
    render(props){
        //console.log(this.props.post)
    return(

        <aside>  
    <h4>Related posts</h4>

    <ul class="related">
   {this.props.post.map((data)=>( 
     <li className="related-post">
             <img
               src={data.img}
               alt="Person programming"
               width="75"
               height="75"
             />
              <div>
          <a href={data.link} className="related-link">{data.title}</a>
          <p className="related-author">By {data.Author}</p>
        </div>
      </li>))}
    
    </ul>
  </aside>
    )
    }

}
export default Aside 