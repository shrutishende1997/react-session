import React,{ Component } from 'react';
import Artcal from './Artical';
import Aside from './Aside';
import Header from './Header';
import Footer from './Footer';

class Home extends Component{
    render(){
      const AsideMenu = [{title:'How to Learn Web Development',
                          Author:'Jonas Schmedtmann',
                          img:'img/related-1.jpg',
                          link:'/blog'
                        },
                         
                          {title:'The Unknown Powers of CSS',
                          Author:'Jim Dillon',
                          img:'img/related-2.jpg',
                          link:'/blog',
                          },

                          {title:'Why JavaScript is Awesome',
                          Author:'Matilda',
                          img:'img/related-3.jpg',
                          link:'/blog'
                        } ] 
                          
       const HeaderMenu = {title:'The Basic Language of the Web: HTML',
                          img:'img/laura-jones.jpg',
                          author:'Posted by <strong>Laura Jones</strong> on Monday, June 21st 2027',
                          banner:'img/post-img.jpg',
                          describe:'All modern websites and web applications are built using threefundamentaltechnologies: HTML, CSS and JavaScript. These are the languages of the web.'
                          }  
                          
                       


        return(
            <>
              <Artcal meanu={HeaderMenu} />
              <Aside post={AsideMenu}/> 
            </>
        )
    }

}

export default Home






// const MyComponents = {
//   DatePicker: function DatePicker(props) {
//     return <div>Imagine a {props.color} datepicker here.</div>;
//   }
// }
