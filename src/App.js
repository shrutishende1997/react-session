import logo from './logo.svg';
import './App.css';
import './components/style.css'
import Home from './components/Home';
import Blog from './components/Content/Blog';
import React,{ Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Flexbox from './components/Content/Flexbox';
import HeaderSection from './components/HeaderSection';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
class App extends Component{
render(){
  const FooterContent = {title:' Copyright &copy; 2027 by The Code Magazine.'} 
  return (<>
            <HeaderSection>
            <Header />
            <BrowserRouter>
             <Routes>
              <Route exact path='/' element={<Home />}></Route>
              <Route path='/blog' element={<Blog />}></Route>
              <Route path='/flex' element={<Flexbox />}></Route>
          </Routes>   
        </BrowserRouter>
        <Footer footer={FooterContent} />
        </HeaderSection>
       
 

    {/* <Home/> */}
  </>)
  }
}
export default App;
